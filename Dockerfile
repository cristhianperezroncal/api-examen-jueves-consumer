FROM openjdk:11



WORKDIR /app

COPY ./target/transactionConsumer-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "transactionConsumer-0.0.1-SNAPSHOT.jar"]