package com.cperez.transactionConsumer.controller;

import com.cperez.transactionConsumer.dto.TransactionDTO;
import com.cperez.transactionConsumer.model.Transaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/api")
@RequiredArgsConstructor
public class TransactionController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public List<Transaction> getTransactions() {
        String url = "http://20.124.167.106:80/v1/transactions";
        ResponseEntity<Transaction[]> response = restTemplate.getForEntity(url, Transaction[].class);
        Transaction[] transactions = response.getBody();
        return Arrays.asList(transactions);
    }

    @PostMapping
    public Transaction createTransaction(@RequestBody TransactionDTO transactionDTO) {
        String url = "http://20.124.167.106:80/v1/transactions";
        Transaction newTransaction = restTemplate.postForObject(url, transactionDTO, Transaction.class);
        return newTransaction;
    }

}
