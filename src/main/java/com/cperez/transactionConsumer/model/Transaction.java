package com.cperez.transactionConsumer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    private String id;
    private Client client;
    private String description;
    private double monto;
    private String estado;
    private LocalDateTime date;

}
