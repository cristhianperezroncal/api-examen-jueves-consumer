package com.cperez.transactionConsumer.dto;

import com.cperez.transactionConsumer.model.Client;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDTO {
    private Client client;
    private String description;
    private double monto;
    private String estado;
    //private LocalDateTime date;

}
